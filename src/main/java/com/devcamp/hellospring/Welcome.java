package com.devcamp.hellospring;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Welcome {
    @CrossOrigin
    @GetMapping("/devcamp-welcome")
    public String nice() {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("hh:mm a");
        return String.format("Have a nice day. It is %s!.", timeFormat.format(now));
    }
}
